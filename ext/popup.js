// Initialize Firebase
  var config = {
    apiKey: "AIzaSyDRbKpTe-msmUTqvIJMHBRQtYe9-Y4XPTI",
    authDomain: "lazycode-4d411.firebaseapp.com",
    databaseURL: "https://lazycode-4d411.firebaseio.com",
    storageBucket: "lazycode-4d411.appspot.com",
    messagingSenderId: "1005535696669"
  };
  firebase.initializeApp(config);

// Get Elements:
  var btnLogin = document.getElementById('btnLogin');
  var footer = document.getElementById('footer');
  var emailField = document.getElementById('userMail');
  var total_counter = document.getElementById('total_counter');
  var total_time = document.getElementById('total_time');
  var local_counter = document.getElementById('local_counter');
  var local_time = document.getElementById('local_time');

  var table = document.getElementById('table');

  var number_field = document.getElementById('number');
  var code_field = document.getElementById('code');
  var time_field = document.getElementById('time');

  var database = firebase.database();

function InsertTr(code) {
  return '					<tr id="\''+code+'\'">'+
  						'<td id="number_\''+code+'\'">\''+code+'\'</td>'+
  						'<td id="code_%code%">%code%</td>'+
  						'<td id="time_%code%" >%code%</td>'+
  						'<td>'+
  							'<div class="btn-group pull-right">'+
  							 '<button onclick="OnDelete(\''+code+'\')" id="delete" type="button" class="btn btn-default btn-xs ">'+
  							 '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+
  								'Delete </button></div>'+
  							'<div class="btn-group pull-right">'+
  							'<button onclick="OnCopy(\''+code+'\')" id="copy" type="button" class="btn btn-default btn-xs ">'+
  							'<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'+
  							 'Copy </button></div>'+
  							'</td>'+
  					'</tr>'  ;
}

function OnCopy(code) {
  var q=$('#'+code+'code_'+code);
  console.log(q);
  $('#table').hide();
  // debugger;
  //q.select
  //q.text
}

  function table_function(visible,number,code,time) {
    if (visible) {
      number_field.innerText = number;
      code_field.innerText = code;
      time_field.innerText = time;
      table.style.display = 'table';
    } else {
      table.style.display = 'none';
    }
  }

function initApp() {
  chrome.storage.local.get('total_counter', function(test){
    console.log(test);
  });

  firebase.database().ref('timeCounter').on('value', function(snap){
    var total_sec = snap.child('receivedSmsCount').val();
    var total_multiplyer = snap.child('savedSec').val();
    total_counter.innerText = total_sec;
    // total_time.innerText = total_sec*total_multiplyer;
  });

  firebase.auth().onAuthStateChanged(function(user){
    if (user) {
      var uid = user.uid;
      emailField.innerText = user.email;
      btnLogin.innerText = 'Log out';
      btnLogin.disabled = false;

      // btnLogin.setAttribute("class", "hide");

      footer.style.display = 'block';

      firebase.database().ref('/users/'+uid+'/timeCounter/').on('value', function(snap){
        var local_sec = snap.child('receivedSmsCount').val();
        var local_multiplyer = snap.child('savedSec').val();
        local_counter.innerText = local_sec;
        // local_time.innerText = local_sec * local_multiplyer;
      });

      firebase.database().ref('/users/'+uid+'/smsList/').on('child_added', function(snap){
        var date = new Date(snap.child('date').val()).toTimeString().split(' ')[0];
        //table_function(true,snap.key,snap.child('code').val(),date);

        $(InsertTr(snap.child('code').val())).appendTo('#table');
      });

      firebase.database().ref('/users/'+uid+'/smsList/').on('child_removed', function(snap){
        table_function(false);
      });

        // var table_hide = setTimeout(function(){
        // }, delay*1000);

      // idField.innerText = uid;
      // console.log(JSON.stringify(user, null, '  '));

    } else {
      btnLogin.innerText = 'Log in';
      emailField.innerText = 'Not logged ln';
      local_counter.innerText = '';
      // local_time.innerText = '';
      footer.style.display = 'none';
      btnLogin.disabled = false;
    }
  });
  btnLogin.addEventListener('click', startSignIn, false);
}

/**
 * Start the auth flow and authorizes to Firebase.
 * @param{boolean} interactive True if the OAuth flow should request with an interactive mode.
 */
function startAuth(interactive) {
  // Request an OAuth token from the Chrome Identity API.
  chrome.identity.getAuthToken({interactive: !!interactive}, function(token) {
    if (chrome.runtime.lastError && !interactive) {
      console.log('It was not possible to get a token programmatically.');
    } else if(chrome.runtime.lastError) {
      console.error(chrome.runtime.lastError);
    } else if (token) {
      // Authrorize Firebase with the OAuth Access Token.
      var credential = firebase.auth.GoogleAuthProvider.credential(null, token);
      firebase.auth().signInWithCredential(credential).catch(function(error) {
        // The OAuth token might have been invalidated. Lets' remove it from cache.
        if (error.code === 'auth/invalid-credential') {
          chrome.identity.removeCachedAuthToken({token: token}, function() {
            startAuth(interactive);
          });
        }
      });
    } else {
      console.error('The OAuth Token was null');
    }
  });
}

/**
 * Starts the sign-in process.
 */
function startSignIn() {
  btnLogin.disabled = true;
  if (firebase.auth().currentUser) {
    firebase.auth().signOut();
  } else {
    startAuth(true);
  }
}

window.onload = function() {
  initApp();
};
