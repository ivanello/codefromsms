  // Initialize Firebase
  var config = {
      apiKey: "AIzaSyDRbKpTe-msmUTqvIJMHBRQtYe9-Y4XPTI",
      authDomain: "lazycode-4d411.firebaseapp.com",
      databaseURL: "https://lazycode-4d411.firebaseio.com",
      storageBucket: "lazycode-4d411.appspot.com",
      messagingSenderId: "1005535696669"
    };
    firebase.initializeApp(config);
//Variables
    var ref = firebase.database().ref();


function log(event) {
  console.log(event);
}

function localStor(oper,key,val) {
  switch(oper) {
  case 'write':
    chrome.storage.local.set({key: val});
    log('written');
    break;
  case 'read':
  chrome.storage.local.get(key, function(val){
      log(val.key);
      return(val.key);
    });
    break;
  default:
    break;
}


}

function extensionActiveIcon(active) {
  if (active) {
    chrome.browserAction.setIcon({
      path: "icon/open48.png"
    });
  } else {
    chrome.browserAction.setIcon({
      path: "icon/close48.png"
    });
  }
}

function notif(number,code,date) {
  var myNotificationID = null;
  timer = setTimeout(function(){chrome.notifications.clear(myNotificationID);}, 1000);
  chrome.notifications.create(
    "",{
        type: "basic",
        iconUrl: "icon/open48.png",
        title: "Lazy Code",
        message: "Template: "+number+"\nCode: "+code+"\nReceived: "+date,
        buttons: [{ title: "Copy code to clipboard",
                    iconUrl: "icon/open48.png"},
                  // { title: "Paste code",
                  //   iconUrl: "icon/open.png"}
                ],
        priority: 2,
        // eventTime: Date.now() + 10,
        isClickable: false,
        // requireInteraction: true,
      },
        function(id) {
          myNotificationID = id;
          console.log("Notif ID:", id);
          console.log("Last notifications error:", chrome.runtime.lastError);
        }
      );

  chrome.notifications.onButtonClicked.addListener(function(notifId, btnIdx) {
      if (notifId == myNotificationID) {
          if (btnIdx === 0) {
              console.log('click: ', notifId, myNotificationID);
              console.log('btn ID: ', btnIdx);
              code.toString();
              // Create a dummy input to copy the string array inside it
              var dummy = document.createElement("input");
              // Add it to the document
              document.body.appendChild(dummy);
              // Set its ID
              dummy.setAttribute("id", "dummy_id");
              // Output the array into it
              document.getElementById("dummy_id").value=code;
              // Select it
              dummy.select();
              // Copy its contents
              document.execCommand("copy");
              // Remove it as its not needed anymore
              document.body.removeChild(dummy);
          } else if (btnIdx === 1) {
            console.log('sorry');
          }
      }
    });
  }

function initApp(){
  ref.child('timeCounter').once('value', function (snap) {
      localStor('write','total_counter',snap.child('receivedSmsCount').val());
      // chrome.storage.local.set({'total_counter':snap.child('receivedSmsCount').val()});
  });



  var removeTimeout;
  firebase.auth().onAuthStateChanged(function(user){
    if (user) {
      var userbd = ref.child('users').child(user.uid).child('smsList');

      userbd.on('child_added', function (snap){
        var date = new Date(snap.child('date').val()).toTimeString().split(' ')[0];
        notif(snap.key,snap.child('code').val(),date);
        clearTimeout(removeTimeout);
        extensionActiveIcon(true);
        console.log(JSON.stringify(snap.val(), null , 3));
        console.log(snap.child('code').val());
        removeTimeout = setTimeout(function(){
          snap.ref.remove(function(error) {
            console.log(error ? "Remove failed: " + error.message : "Remove succeeded.");
            extensionActiveIcon(false);
          });
        }, 60*1000);
      });
    } else {
      log('logged out');
      chrome.storage.local.get('total_counter', function(val){
          log(val.total_counter);
        });
    }
  });
}

window.onload = function() {
  initApp();
};
