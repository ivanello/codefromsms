/*---------------------------------------------------------------------------------*/
function showDate(GMTdate)
{
var d = new Date(GMTdate + " GMT");
var day = d.getDate()+"";
if(day.length == 1) day = "0" + day;
var month = d.getMonth()+1+"";
if(month.length == 1) month = "0" + month;
var year = d.getFullYear();
var hour = d.getHours()+"";
if(hour.length == 1) hour = "0" + hour;
var min = d.getMinutes()+"";
if(min.length == 1) min = "0" + min;
var sec = d.getSeconds()+"";
if(sec.length == 1) sec = "0" + sec;
document.write(day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec);
}
/*---------------------------------------------------------------------------------*/
function notNull(param)
{
var value = eval("document.submitForm."+param+".value");
if(value == null || value.length == 0){
alert(nullErrMsg);
eval("document.submitForm."+param+".value='';");
eval("document.submitForm."+param+".focus();");
return false;
}
return true;
}
/*---------------------------------------------------------------------------------*/
function checkLength(param, min, max)
{
var value = eval("document.submitForm."+param+".value");
if(value.length < min || value.length > max){
alert(lengthMsg);
eval("document.submitForm."+param+".value='';");
return false;
}
return true;
}
/*---------------------------------------------------------------------------------*/
function checkDigit(param, mindigit)
{
var value = eval("document.submitForm."+param+".value");
var digits = 0;

if(value != null && mindigit > 0)
{
  for(var i = 0; i < value.length;i++){
      if(value.charAt(i).match("[0-9]") != null)
	digits++;	
  }
}
if (digits < mindigit)
{
alert(digitMsg);
eval("document.submitForm."+param+".value='';");
return false;
}
return true;
}
/*---------------------------------------------------------------------------------*/
function checkCapital(param, mincapital)
{
var value = eval("document.submitForm."+param+".value");
var capitals = 0;

if(value != null && mincapital > 0)
{
  for(var i = 0; i < value.length;i++){
      if(value.charAt(i).match("[A-Z]") != null)
	capitals++;	
  }
}
if (capitals < mincapital)
{
alert(capitalMsg);
eval("document.submitForm."+param+".value='';");
return false;
}
return true;
}
/*---------------------------------------------------------------------------------*/
function matchesField(field1, field2)
{
var value1 = eval("document.submitForm."+field1+".value");
var value2 = eval("document.submitForm."+field2+".value");
if(value1 != value2)
{
alert(confirmMsg);
eval("document.submitForm."+field1+".value='';");
eval("document.submitForm."+field2+".value='';");
return false;
}
return true;
}
/*---------------------------------------------------------------------------------*/