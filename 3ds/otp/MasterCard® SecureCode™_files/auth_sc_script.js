/* Prevent double clicks */
/*---------------------------------------------------------------------------------*/
var help_click;
var lang_click;
var cancel_click;
var submit_click;
var opt_out_click;
var devReg_click;
var syncLink_click;
var chg_pass_click;
var reset_click;
var ok_click;
var back_click;
var close_click;
var terms_click;
var prev_click;
var needToConfirm = true;
var allowSubmit = true;
var otp_click;

function attachClicks () { 
    evalAll(0);
    if(document.getElementById("cancelLink")){
       document.getElementById("cancelLink").onclick = function(){
			if(cancel_click == 0){
				evalAll(1);
				document.cancelForm.submit();
			} 
			return false;
		};
	}
	if(document.getElementById("sms_cancelLink")){
       document.getElementById("sms_cancelLink").onclick = function(){
			if(cancel_click == 0){
				evalAll(1);
				document.cancelForm.submit();
			} 
			return false;
		};
	}
	if(document.getElementById("do_submit_btn")){
		document.getElementById("do_submit_btn").onclick = function(){
			doSubmit();
			return false;
		};
	}
	if(document.getElementById("submit_act_btn")){
		document.getElementById("submit_act_btn").onclick = function(){
			if(submit_click == 0){
				evalAll(1);
				submitAction();
			}
			return false;
		};
	}
    if(document.getElementById("opt_outLink")){
		document.getElementById("opt_outLink").onclick = function(){
			if(opt_out_click == 0){
				evalAll(1);
				document.cancelForm.submit();
			} 
			return false;
		};
	} 
	if(document.getElementById("submit_btn")){
		document.getElementById("submit_btn").onclick = function(){
			if(submit_click == 0){
				evalAll(1);
				document.submitForm.submit();
			} 
			return false;
		};
	}
	if (document.getElementById("syncLink")) { 
		document.getElementById("syncLink").onclick = function(){
			if(syncLink_click == 0){
				evalAll(1);
				document.syncForm.submit();
			} 
			return false;
		};
	} 
	if(document.getElementById("otpLink")){
		document.getElementById("otpLink").onclick = function(){
			if(otp_click==0){
				evalAll(1);
				document.requestOtpForm.submit();
			}
			return false;
		};
    }
	if(document.getElementById("resetLink")){   
		document.getElementById("resetLink").onclick = function(){
			if(reset_click == 0){
				evalAll(1);
				document.resetForm.submit();
			} 
			return false;
		};
	}
	if(document.getElementById("backLink")){
		document.getElementById("backLink").onclick = function(){
			if(submit_click==0){
				evalAll(1);
				document.backForm.submit();
			} 
			return false;
		};
	}  
	if(document.getElementById("close_btn")){
       document.getElementById("close_btn").onclick = function(){
			if(close_click==0){
				evalAll(1);
				window.close();
			} 
			return false;
		};
	} 
	if (document.getElementById("reset_btn")) { 
		document.getElementById("reset_btn").onclick = function(){
			if(reset_click == 0){
				evalAll(1);
				document.resetForm.submit();
			} 
			return false;
		};
    } 
	if(document.getElementById("devRegLink")){
		document.getElementById("devRegLink").onclick = function(){
			if(devReg_click == 0){
				evalAll(1);
				chooseAction();
			}
		};
	}
	if(document.getElementById("regLink")){
		document.getElementById("regLink").onclick = function(){
			if(devReg_click == 0){
				evalAll(1);
				registerAction();
			}
		};
	}
	if(document.getElementById("reg_btn")){
		document.getElementById("reg_btn").onclick = function(){
			if(devReg_click == 0){
				evalAll(1);
				registerAction();
			}
		};
	}
}

function evalAll(value) {
	submit_click = value;
	cancel_click = value;
	lang_click = value;
	help_click = value;
	opt_out_click = value;
	devReg_click = value;
	syncLink_click = value;
	chg_pass_click = value;
    reset_click = value;
	ok_click = value;
	back_click = value;
	close_click = value;
	terms_click=value;
	prev_click = value;
	otp_click = value;
	
	if(value == 1){
		needToConfirm = false;
		disableLinks();
	}
}

function disableLinks(){
	if (document.getElementById("syncLink")) { 
		if(document.getElementById("syncLink").style.display!="none"){
			document.getElementById("syncLink").style.display="none";
			document.getElementById("sync_link").style.display="inline";
			document.getElementById("sync_link").onclick = function(){
				return false;
			};
		}
	}
	if (document.getElementById("otpLink")) { 
		if(document.getElementById("otpLink").style.display!="none"){
			document.getElementById("otpLink").style.display="none";
			document.getElementById("otp_link").style.display="inline";
			document.getElementById("otp_link").onclick = function(){
				return false;
			};
		}
	}
	if(document.getElementById("cancelLink")){
		if(document.getElementById("cancelLink").style.display!="none"){
			document.getElementById("cancelLink").style.display = "none";
			document.getElementById("cancel_link").style.display = "inline";
			document.getElementById("cancel_link").onclick = function(){
				return false;
			};
		}
	}
	if(document.getElementById("sms_cancelLink")){
		if(document.getElementById("sms_cancelLink").style.display!="none"){
			document.getElementById("sms_cancelLink").style.display = "none";
			document.getElementById("sms_cancel_link").style.display = "inline";
			document.getElementById("sms_cancel_link").onclick = function(){
				return false;
			};
		}
	}
	if (document.getElementById("helpLink")) { 
		document.getElementById("helpLink").style.display = "none";
		document.getElementById("help_link").style.display = "inline";
		document.getElementById("help_link").onclick = function(){
			return false;
		};
    }
    if (document.getElementById("langLink_def")) { 
		document.getElementById("langLink_def").style.display = "none";
		document.getElementById("lang_link_def").style.display = "inline";
		document.getElementById("lang_link_def").onclick = function(){
			return false;
		};
    }
    if (document.getElementById("langLink_sec")) { 
		document.getElementById("langLink_sec").style.display = "none";
		document.getElementById("lang_link_sec").style.display = "inline";
		document.getElementById("lang_link_sec").onclick = function(){
			return false;
		};
    }
	if (document.getElementById("submit_btn")) { 
		document.getElementById("submit_btn").style.display = "none";
		document.getElementById("submit_disabled").style.display = "inline";
		document.getElementById("submit_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("reg_submit_btn")) { 
		document.getElementById("reg_submit_btn").style.display = "none";
		document.getElementById("reg_submit_disabled").style.display = "inline";
		document.getElementById("reg_submit_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("submit_act_btn")) { 
		document.getElementById("submit_act_btn").style.display = "none";
		document.getElementById("submit_act_disabled").style.display = "inline";
		document.getElementById("submit_act_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("do_submit_btn")) { 
		document.getElementById("do_submit_btn").style.display = "none";
		document.getElementById("do_submit_disabled").style.display = "inline";
		document.getElementById("do_submit_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("opt_outLink")) { 
		document.getElementById("opt_outLink").style.display = "none";
		document.getElementById("opt_out_link").style.display = "inline";
		document.getElementById("opt_out_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("devRegLink")) { 
		document.getElementById("devRegLink").style.display="none";
		document.getElementById("devReg_link").style.display="inline";
		document.getElementById("devReg_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("regLink")) { 
		document.getElementById("regLink").style.display="none";
		document.getElementById("reg_link").style.display="inline";
		document.getElementById("reg_link").onclick = function(){
			return false;
		};
	}
   if (document.getElementById("reg_btn")) { 
		document.getElementById("reg_btn").style.display="none";
		document.getElementById("reg_disabled").style.display="inline";
		document.getElementById("reg_disabled").onclick = function(){
			return false;
		};
	}
    if (document.getElementById("chg_passLink")) { 
		if(document.getElementById("chg_passLink").style.display!="none"){
			document.getElementById("chg_passLink").style.display="none";
			document.getElementById("chg_pass_link").style.display="inline";
			document.getElementById("chg_pass_link").onclick = function(){
				return false;
			};
		}
	}
	if (document.getElementById("prevLink")) { 
		document.getElementById("prevLink").style.display="none";
		document.getElementById("prev_link").style.display="inline";
		document.getElementById("prev_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("resetLink")) { 
		document.getElementById("resetLink").style.display="none";
		document.getElementById("reset_link").style.display="inline";
		document.getElementById("reset_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("ok_btn")) { 
		document.getElementById("ok_btn").style.display="none";
		document.getElementById("ok_disabled").style.display="inline";
		document.getElementById("ok_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("backLink")) { 
		document.getElementById("backLink").style.display="none";
		document.getElementById("back_link").style.display="inline";
		document.getElementById("back_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("close_btn")) { 
		document.getElementById("close_btn").style.display="none";
		document.getElementById("close_disabled").style.display="inline";
		document.getElementById("close_disabled").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("termsLink")) { 
		document.getElementById("termsLink").style.display="none";
		document.getElementById("terms_link").style.display="inline";
		document.getElementById("terms_link").onclick = function(){
			return false;
		};
	}
	if (document.getElementById("reset_btn")) { 
		document.getElementById("reset_btn").style.display="none";
		document.getElementById("reset_disabled").style.display="inline";
		document.getElementById("reset_disabled").onclick = function(){
			return false;
		};
	}
}
/*******************************************************************/
/* Show buttons and links when javaScript is enabled */
/*******************************************************************/
function showElements(){
	elements = getElementsByBaseId("links_");
	for(var i = 0; i < elements.length ; i++ ){
		elements[i].style.display="inline";
	}
}
/*******************************************************************/
/* Return Wanted Elements */
/*******************************************************************/
function getElementsByBaseId(baseIdentifier) {
    var allWantedElements = [];
	var idMod = 1;
	/* It would be better to use while and loop until the last found element. 
	 * Since the ids are not sequential in the page, loop up to 20 (now MAX idMod is 11 in our pages) */
	while (idMod < 20) {
		if(document.getElementById(baseIdentifier + idMod)) {
			allWantedElements.push(document.getElementById(baseIdentifier + idMod));
		}
		idMod++;
    }
    return allWantedElements;
}
/*******************************************************************/
/* Do run this when loading page */
/*******************************************************************/
function doOnload(){
	showElements();
	attachClicks();
}
/****************************************************/

